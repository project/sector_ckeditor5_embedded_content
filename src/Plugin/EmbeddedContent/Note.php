<?php

namespace Drupal\sector_ckeditor5_embedded_content\Plugin\EmbeddedContent;

use Drupal\ckeditor5_embedded_content\EmbeddedContentInterface;
use Drupal\ckeditor5_embedded_content\EmbeddedContentPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin note.
 *
 * @EmbeddedContent(
 *   id = "note",
 *   label = @Translation("Note"),
 *   description = @Translation("Renders a Note component."),
 * )
 */
class Note extends EmbeddedContentPluginBase implements EmbeddedContentInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'heading' => NULL,
      'message' => NULL,
      'type' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return [
      '#theme' => 'note',
      '#data' => [
        'heading' => $this->configuration['heading'],
        'message' => $this->configuration['message'],
        'type' => $this->configuration['type'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['heading'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Heading'),
      '#default_value' => $this->configuration['heading'],
      '#description' => $this->t('Renders inside a h4 element.'),
    ];

    $form['message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message'),
      '#default_value' => $this->configuration['message'],
      '#required' => TRUE,
    ];

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#default_value' => $this->configuration['type'],
      '#required' => TRUE,
      '#options' => [
        'note--default' => $this->t('Default (teal)'),
        'note--alert' => $this->t('Alert (yellow)'),
        'note--info' => $this->t('Info (blue)'),
        'note--danger' => $this->t('Danger (red)'),
      ]
    ];

    return $form;
  }

}
